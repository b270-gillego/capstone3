const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "first name is required"]
	},
	lastName: {
		type: String,
		required: [true, "last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},

	orders: [{
		products: [{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			quantity: {
				type: Number,
				default: 0
			}
		}],
		totalAmount: {
			type: Number
		},
		purchasedOn: {
			type: Date,
			dafault: new Date()
		}
	}]
})

module.exports = mongoose.model("User", userSchema);