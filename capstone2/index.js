const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");


// Server setup
const app = express();

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");


// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.cizqn6d.mongodb.net/Ecommerce?retryWrites=true&w=majority",

	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}	
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("We're connected to the ecommerce database"))

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);

// Server listening
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
});