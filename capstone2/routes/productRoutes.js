const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


router.post("/", auth.verify, productController.createProduct);

router.get("/all", auth.verify, productController.getAllProducts);

router.get("/", productController.getAllActive);

router.get("/:productId", productController.getProduct);

router.put("/:productId", auth.verify, productController.updateProduct);

router.patch("/:productId/archive", auth.verify, productController.archiveProduct);

module.exports = router;
