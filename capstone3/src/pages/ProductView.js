import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  const { productId } = useParams();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [stocks, setStocks] = useState(0);
  const [quantity, setQuantity] = useState(1);

  const buy = () => {
    const checkoutData = {
      userId: user.id,
      products: [{ productId: productId, quantity: quantity }],
    };

    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(checkoutData),
    })
      .then(response => response.json())
      .then(data => {
        if (data) {
          Swal.fire({
            title: "Successful checkout!",
            icon: "success",
            text: "You have successfully purchased a product."
          });
          navigate("/products");

        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again."
          });
        }
      })
      .catch(error => {
        console.error("Error:", error);
        Swal.fire({
          title: "Error",
          icon: "error",
          text: "An error occurred during checkout."
        });
      });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then(response => response.json())
      .then(data => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setStocks(data.stocks);
      })
      .catch(error => {
        console.error("Error:", error);
        Swal.fire({
          title: "Error",
          icon: "error",
          text: "An error occurred while fetching product details."
        });
      });
  }, [productId]);

  return (
    <Container>
      <Row>
        <Col>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              <Card.Subtitle>Stocks:</Card.Subtitle>
              <Card.Text>{stocks}</Card.Text>
              {user.id !== null 
                ? (
                  <>
                  <Form.Group className="mx-auto col-1 text-center" controlId="quantity">
                  <Form.Label>Quantity</Form.Label>
                  <Form.Control
                    className="text-center"
                    type= "number"
                    min= "1"
                    value= {quantity}
                    onChange={(e) => setQuantity(parseInt(e.target.value))}/>
                  </Form.Group>
                <Button variant="primary" onClick={buy}>Buy</Button>
                  </>)
                :(
                  <Button variant="danger" as={Link} to="/login">Log in to Purchase</Button>)
             }
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}