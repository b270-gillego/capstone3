import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
	    title: "Dependable Fire Fighting Solutions",
	    content: "Your fire fighting needs, in your hands",
	    destination: "/products",
	    label: "Shop now!"
	}
	
	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}