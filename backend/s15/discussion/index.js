// alert("Hello, Batch 270!");

// [SECTION] Syntax, Statements and Comments
// Statements in programming are instructions that we tell the computer to perform
// JS statements usually end with semicolon (;)
// Semicolons are not required in JS, but we will use it to help us train to locate where a statement ends
// A syntax in programming, it is the set of rules that describes how statements must be constructed
// All lines/blocks of code should be written in a specific manner to work. This is due to how these codes were initially programmed to function and perform in a certain manner

// This is a statement.
console.log("Hello again!");
console.log("Hi, Batch 270!");

/*
	- There are two types of comments:
		1. Single-line denotated by two slashes.
		2. Multi-line denoted by a slash and asterisk (CTRL + SHIFT + /)
*/


//  [SECTION] Variables
	/*
		- It is used to contain data.

	*/

// Declaring and initializing variables

// Declaring Variables
// Syntax: let/const variableName;
/*
	- Trying to print out a value of a variable that has not been declared will return an error of "undefined"
	- Variables must be declared first with value before they can be used
*/
	let myVariable;
	console.log(myVariable);

	let greeting = "Hello";
	console.log(greeting);


// Initializing variables - the instance when a variable is given its initial value
// Syntax: let/const variableName = value;
// let keyword is used if we want to reassign values to our variable
let productName = 'desktop computer';
productName = "Laptop";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
// interest = 2.5;
console.log(interest);

let friend = "Kate";
console.log(friend);

// we cannot re-declare variables within its scope
// let friend = "Jay";
// console.log(friend)




// [SECTION] Data Types

// String - series of characters that create a word, a phrase, a sentence or anything related to creating a text
// Strings can be written using either a single or double quote.
let country = "Philippines";
let province = "Batangas";
console.log(country);
console.log(province);

// Concatenating strings
// Multiple string values can be combined to create a single string using "+" symbol
let fullAddress = province + ", " + country;
console.log(fullAddress);

console.log("I live in the " + country);

let address = "I live in the " + country;
console.log(address);

// "\n" refers to creating a new line in between text
let mailAddress = 'Metro Manila\nPhilippines';
console.log(mailAddress);

let message = "John's employees went home early.";
console.log(message);

// Numbers
	// Integers/Whole Numbers
	let headCount = 26;
	console.log(headCount);

	// Decimal Numbers
	let grade = 98.7;
	console.log(grade);

	// Exponential Notation
	let planetDistance = 2e10;
	console.log(planetDistance);

// Boolean - value is either true or false

let isMarried = false;
let inGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays - are special kind of data type that's used to store multiple
// Arrays can store different data types but is normally used to store similar data types
// Syntax: let/const arrayName  = [elementA, elementB, ...]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
// Objects are another special kind of data type that's used to mimic real world objects/items
// They're used to create complex data that contains pieces of information that are relevant to each other
// Every individual piece of information is called a property of the object
/*
	 Syntax: 
	 let/const objectName = {
		propertyA: valueA,
		propertyB: valueB		
	 }
*/


let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["0912345678", "09876654321"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
};
console.log(person);

// Null

let spouse = null;
console.log(spouse);

/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
*/