const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 3000;

// change the admin password:
/*
	MongoDB atlas > Database Access (left navigation pane) > look for the admin user > change password
*/
/*
	mongoose.connect - allows our application to be connected to MongoDB
	useNewUrlParser : true - allows us to avoid any current and future errors while connecting to Mongo DB
	useUnifiedTopology : true - it allows us to connect to MongoDB even if the required url is updated
*/
mongoose.connect("mongodb+srv://admin:admin123@zuitt.e1gpuus.mongodb.net/b270-to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}	
);

// connection to the database
// set notifications if the connection is a success or failure
const db = mongoose.connection;
// if a connection error occured, output in the console
// console.error.bind(console) allows us to print errors in the browser console as well as in the terminal
// "connection error" is the message that will display if this happens
db.on("error", console.error.bind(console, "connection error"))
// if the connection is successful, output in the console 'We are connected to the database'
db.once("open", () => console.log('We are connected to the database'));

// [Section] Mongoose Schema
// Schemas determine the structure of documents to be written in the database; they act as blueprints of our data
/*
Use the Schema() constructor of the mongoose module to create a new Schema object
*/
const taskSchema = new mongoose.Schema({
	// define the fields with corresponding data type
	// it needs a task "name" and task "status"
	// the "name" field requires a String data type for its value
	name : String,
	// "status" field requires a String data type, but since we have default : "pending", users can leave this blank with a default value of "pending"
	status : {
		type : String,
		default : "pending"
	}
})

// User Schema
const userSchema = new mongoose.Schema({
    username : String,
    password : String
})

// [Section] Models
// Models use Schemas and they act as the middleman from the server to our database Server > Schema (blueprint) > Database > Collection
// "Task" variable can now be used to run commands for interacting with our database; the naming convention for mongoose models follows the MVC format
// the first parameter of the model() indicates the collection to store the data that will be created
// the second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
const Task = mongoose.model("Task", taskSchema);

// The variable/object "User" can now used to run commands for interacting with our database
const User = mongoose.model("User", userSchema);

// [Section] Creation of todo list routes
// allows our app to read json data
app.use( express.json() );
// allows our app to read data from forms
app.use( express.urlencoded( { extended:true } ) )

// create a new task
// Business Logic
/*
	1. check if the task is already existing in the collection
		- if it exists, return an error/notice.
		- if it's not, we add it in the database
	2. the task data will be coming from the request body
	3. create a new Task object with a "name" field/property
	4. the "status" property does not need to be provided because our schema defaults it to "pending"
*/
app.post('/tasks', (req, res) => {
	// checking for the duplicate tasks
	Task.findOne({ name : req.body.name }).then((result, err)=>{
		// if it exists, return an error/notice.
		if (result != null && result.name === req.body.name){
			return res.send("Duplicate task found");
		// if no document was found
		}else{
			// create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			})
			// save the object in the collection
			newTask.save().then((savedTask, error) => {
				if (error){
					return console.error(error)
				}else{
					return res.status(201).send("New Task Created")
				}
			})
		}
	})
})

// getting all tasks
/*
Business logic:
	1. Retrieve all the documents in the collection (find() method)
	2. if an error is encountered, print the error
	3. if no errors are found, send a success (200) status back to the client/postman and return the array of document/result
	solution:8:56 pm; you can send ss to our batch google chat
*/
app.get('/tasks', (req, res)=>{
	// retrieve all documents
	Task.find({  }).then((result, err)=>{
		// error handling
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- If the user already exists in the database, we return an error
	- If the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Create a new User object with a "username" and "password" fields/properties
*/

// Route for creating a user
app.post("/signup", (req, res)=> {

	// Finds for a document with the matching username provided in the client/Postman
	User.findOne({ username : req.body.username }).then((result, err) => {

		// Check for duplicates
		if(result != null && result.username == req.body.username){

			return res.send("Duplicate username found");

		// No duplicates found
		} else {

			// If the username and password are both not blank
			if(req.body.username !== '' && req.body.password !== ''){

				// Create/instantiate a "newUser" from the "User" model
                let newUser = new User({
                    username : req.body.username,
                    password : req.body.password
                });
    
    			// Create a document in the database
                newUser.save().then((savedUser, saveErr) => {

                    // If an error occurred
                    if(saveErr){

                    	// Return an error in the client/Postman
                        return console.error(saveErr);

                    // If no errors are found
                    } else {

                    	// Send a response back to the client/Postman of "created"
                        return res.status(201).send("New user registered");

                    }

                })

            // If the "username" or "password" was left blank
            } else {

            	/// Send a response back to the client/Postman of "created"
                return res.send("BOTH username and password must be provided.");
            }			
		}
	})
})

app.listen(port, () => console.log(`Server running at  ${port}`) );