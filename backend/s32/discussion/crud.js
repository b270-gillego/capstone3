let http = require('http');
let port = 4000;

// mock database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Taylor",
		"email" : "tswift@mail.com"
	}
]

let server = http.createServer((request, response) => {
	// checks if the url and the method matches the required condition
	if (request.url == "/users" && request.method == "GET"){
		// content-type is set to application/json because in real-world management of databases, we are working with objects and less texts.
		response.writeHead(200, {"Content-Type" : "application/json"});
		// the frontend and backend applications communicate through strings. we have to convert the JSON data type from the server into stringified JSON when it is sent to the frontend application
		response.write(JSON.stringify(directory));
		// ends the response process
		response.end();
	}

	if (request.url == "/users" && request.method == "POST"){
		// created a requestBody variable to an empty string
		let requestBody = '';
		
		 	// In Node.js, 'request.on' is a method that is used to register event listeners for events emitted by an HTTP request object.
			 	/*
			 	Takes two arguments:
					- the event name
					- a callback function/event handler to be executed when the event is emitted.
				*/
			
			// When a request with a body is received by the server, Node.js streams the request body in chunks. The 'data' event is emitted for each chunk of data received by the server.


		// 1st step
		// "data" step - data is received from the client and is processed in the "data" stream
		request.on('data', function(data){

			// Assigns the data retrieved from the data stream to requestBody
			requestBody += data;
			console.log(requestBody)
		});

		// 2nd step
		// "end" step - only runs after the request has completely been sent
		request.on('end',function(){

			console.log(typeof requestBody);

			// converts the stringified JSON to JS Object
			requestBody = JSON.parse(requestBody);

			
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}
			
			console.log(typeof newUser)
			// adding the newly created object to the directory/mock database
			directory.push(newUser);
			console.log(newUser);

			response.writeHead(200, {'Content-Type': 'application/json'})
			response.write(JSON.stringify(newUser));
			response.end();
		})
	}
})
server.listen(port);
console.log(`Server running at localhost:${port}`);