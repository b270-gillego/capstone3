// Exponent Operator
const getCube = 2 ** 3;

// Template Literals
console.log(`The cube of 2 is ${getCube}`);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

const [ houseNumber, street, state, zipCode ] = address;

console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const { name, species, weight, measurement } = animal;

console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => console.log(number));

/*
	- The "accumulator" parameter in the function stores the result for every iteration of the loop
	- The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
	
	- How the "reduce" method works
	    1. The first/result element in the array is stored in the "accumulator" parameter
	    2. The second/next element in the array is stored in the "currentValue" parameter
	    3. An operation is performed on the two elements
	    4. The loop repeats step 1-3 until all elements have been worked on
*/
let reduceNumber = numbers.reduce((x, y) => x + y);

console.log(reduceNumber);

// Javascript Classes
class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");

console.log(myDog);