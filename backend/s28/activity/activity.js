// Inserting a single document
db.rooms.insertOne({
	name: "single",
    accomodates: 2,
    price: 1000,
    description: "A simple room with all the basic necessities",
    roomsAvailable: 10,
    isAvailable: false
})

// Inserting multiple documents
db.rooms.insertMany([
	{
		name: "double",
        accomodates: 3,
        price: 2000,
        description: "A room fit for a small family going on a vacation",
        roomsAvailable: 5,
        isAvailable: false
	},
	{
		name: "queen",
		accomodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	}
	]
)

// Find a document
db.rooms.find({ name: "double" });

// Updating a document
db.rooms.updateOne(
	{name: "queen"},
	{
		$set: {
			roomsAvailable: 0,
		}
	}
)

// Deleting multiple documents
db.rooms.deleteMany({roomsAvailable: 0});